var pizzas = [];
var btnAdicionar = document.getElementById("idButtonAdicionar");
btnAdicionar.onclick = function () {
    let pizza = [];
    let tipo = document.getElementById("idNomeComercial").value;
    let tamanho = Number(document.getElementById("idTamanhoPizza").value);
    let preco = Number(document.getElementById("idPrecoPizza").value);
    let precoCm2 = calcularPrecoCm2(preco, tamanho);

    pizza.push(tipo, tamanho, preco, precoCm2);
    pizzas.push(pizza);
    console.log(pizzas);
    ordenaPizzasPorMenorPreco();

}

console.log(pizzas);

function calcularPrecoCm2(preco, tamanho) {
    var area = 3.14 * (tamanho / 2) ** 2
    var precoCm2 = preco / area
    return precoCm2
}

function ordenaPizzasPorMenorPreco() {
    pizzas.sort(function (a, b) {
        if (a[3] < b[3]) {
            return -1;
        }
        if (a[3] > b[3]) {
            return 1;
        }
        return 0;
    })

}
var btnCalcular = document.getElementById("idButtonCalcular");
btnCalcular.onclick = function () {

    calculaCb();
    montaTabela();

}

function montaTabela() {
    let tbody = document.getElementById("idTbodyResultados");
    for (let index = 0; index < pizzas.length; index++) {

        tbody.appendChild(montaTr(pizzas[index]))


    }

}

function montaTr(pizza) {
    let tr = document.createElement("tr");
    console.log(calculaCb);
    tr.appendChild(montaTd(pizza[0], "tdNome"))
    tr.appendChild(montaTd(pizza[1], "tdTamanho"))
    tr.appendChild(montaTd(pizza[2], "tdPreco"))
    tr.appendChild(montaTd(pizza[3], "tdValor"))
    if (isNaN(pizza[4])) {
        tr.appendChild(montaTd(pizza[4], "tdDiferenca"))
    } else {
        tr.appendChild(montaTd(pizza[4], "tdDiferenca"))
    }

    return tr;

}

function montaTd(valor, classe) {
    let td = document.createElement("td");
    td.classList.add(classe);
    td.textContent = valor;
    return td;

}

function calculaCb() {
    pizzas[0].push("Melhor CB")
    for (let i = 0; i < pizzas.length - 1; i++) {
        var precoCm2A = pizzas[i][3]
        var precoCm2B = pizzas[i + 1][3]

        var porcentagem = ((precoCm2B / precoCm2A) - 1) * 100

        pizzas[i + 1].push(porcentagem)
    }

    return porcentagem;

    
}

